using UnityEngine;

public static class GlobalFunctions 
{
    public static int RandomProtectOfSameValue(int min, int max, int ctx = -1)
    {
        if (ctx == -1) return Random.Range(min, max);
        int temp = Random.Range(min, max);
        if (temp == ctx)
            return RandomProtectOfSameValue(min, max, ctx);
        return temp;
    }
}
