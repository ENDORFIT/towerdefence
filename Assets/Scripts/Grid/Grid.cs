using UnityEngine;

namespace Grids
{
    public class Grid
    {

        private Vector3[,] _gridArrayCoordinate;

        private int _width, _height;
        private Vector3 _offset;
        private Vector3 _coordinates;

        private CreateGridMesh _creationGridMesh;
        private CreateGridPath _createGridPath;

        public Grid (int ctxWidth, int ctxHeight, Vector3 ctxCoords, Vector3 ctxOffest, GameObject ctxPathPrefab, GameObject ctxEnvironmentPrefab)
        {
            _width = ctxWidth;
            _height = ctxHeight;
            _offset = ctxOffest;
            _coordinates = ctxCoords;

            InitializationGridArrayCoordinates();

            _createGridPath = new CreateGridPath(this);
            _creationGridMesh = new CreateGridMesh(this, ctxPathPrefab, ctxEnvironmentPrefab);
        }

        public Vector3 Offset() => _offset;
        public int Width() => _width;
        public int Height() => _height;
        public int[,] GridRoadIndex() => _createGridPath.GridRoadIndex();
        public Vector3[,] GridArrayCoordinate() => _gridArrayCoordinate;


        private void InitializationGridArrayCoordinates()
        {
            _gridArrayCoordinate = new Vector3[_height, _width];
            for (int y = 0; y < _height; y++)
            {
                for (int x = 0; x < _width; x++)
                {
                    _gridArrayCoordinate[y, x] = new Vector3(_coordinates.x + _offset.x * (y + 1), _coordinates.y, _coordinates.z + _offset.z * (x + 1));
                }
            }
        }
    }
}
