using UnityEngine;


namespace Grids
{
    public class LoadGrid : MonoBehaviour
    {
        [Header("Prefabs: ")]
        [SerializeField] GameObject _pathPrefab = null;
        [SerializeField] GameObject _environmentPrefab = null;

        [Header("Properties: ")]
        private Vector3 _startPosition = new Vector3(0,0,0);
        private Vector3 _offset = new Vector3(10, 1, 10);
        private int _width = 20;
        private int _height = 20;

        private Grid _grid;


        private void Start()
        {
            _grid = new Grids.Grid(_width, _height, _startPosition, _offset, _pathPrefab, _environmentPrefab);
        }


    }
}