using UnityEngine;


namespace Grids
{

    public class CreateGridMesh
    {
        private Grid _gridProperties;
        GameObject _pathPrefab = null;
        GameObject _environmentPrefab = null;

        public CreateGridMesh (Grid ctxGrid, GameObject ctxPathPrefab, GameObject ctxEnvironmentPrefab)
        {
            _gridProperties = ctxGrid;

            _pathPrefab = ctxPathPrefab;
            _environmentPrefab = ctxEnvironmentPrefab;

            InitializationGridMeshes();
        }

        private void InitializationGridMeshes()
        {
            for (int y = 0; y < _gridProperties.Height(); y++)
            {
                for (int x = 0; x < _gridProperties.Width(); x++)
                {
                    if (_gridProperties.GridRoadIndex()[x,y] == 1) GameObject.Instantiate(_pathPrefab, _gridProperties.GridArrayCoordinate()[y,x], Quaternion.identity);
                    else GameObject.Instantiate(_environmentPrefab, _gridProperties.GridArrayCoordinate()[y, x], Quaternion.identity);
                }
            }
        }


    }
}