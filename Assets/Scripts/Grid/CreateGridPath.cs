using UnityEngine;

namespace Grids
{
    public class CreateGridPath // �������� ���� ��� ����������� enemies
    {
        private Grid _gridProperties;

        private int[,] _gridRoadIndex; // 0 - �����, 1 - ����

        private int _startRoad_X = 0, _startRoad_Y = 0; 
        private int _endRoad_X = 0, _endRoad_Y = 0;
        private int _inaccuracy = 4;

        public CreateGridPath (Grid ctxGrid)
        {
            _gridProperties = ctxGrid;

            _startRoad_X = GlobalFunctions.RandomProtectOfSameValue(0, _gridProperties.Width());
            _endRoad_X = GlobalFunctions.RandomProtectOfSameValue(0, _gridProperties.Width(), _startRoad_X);
            _endRoad_Y = _gridProperties.Height() - 1;

            _gridRoadIndex = new int[_gridProperties.Height(), _gridProperties.Width()];

            InitializationGridPath();
        }

        public int[,] GridRoadIndex() => _gridRoadIndex;


        private void InitializationGridPath () {
            _gridRoadIndex[_startRoad_Y, _startRoad_X] = 1;

            int curX = _startRoad_X, curY = _startRoad_Y + 1;
            _gridRoadIndex[curY, curX] = 1;

            while (curX != _endRoad_X || curY != _endRoad_Y)
            {
                if (curX == _endRoad_X)
                {
                    if (!(curY + _inaccuracy >= _endRoad_Y))
                    {
                        _endRoad_X = GlobalFunctions.RandomProtectOfSameValue(0, _gridProperties.Width(), curX);
                        if (curY != _endRoad_Y)
                        {
                            curY++;
                            _gridRoadIndex[curY, curX] = 1;
                        }
                    }
                    curY++;
                }
                else if (curY != _endRoad_Y) 
                {
                    if (curX > _endRoad_X) curX--;
                    else curX++;
                    if (Random.Range(0, 2) == 1)
                    {
                        _gridRoadIndex[curY, curX] = 1;
                        curY++;
                    }
                }
                else
                {
                    if (curX > _endRoad_X) curX--;
                    else curX++;
                }

                _gridRoadIndex[curY, curX] = 1;
            }
        }
    }
}