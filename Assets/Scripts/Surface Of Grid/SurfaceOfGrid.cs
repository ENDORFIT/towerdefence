using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfaceOfGrid : MonoBehaviour
{
    [SerializeField] GameObject _selectedObject = null;

    private void Awake()
    {
        _selectedObject.SetActive(false);
    }

    private void OnMouseEnter()
    {
        _selectedObject.SetActive(true);
    }
    private void OnMouseExit()
    {
        _selectedObject.SetActive(false);
    }
}
